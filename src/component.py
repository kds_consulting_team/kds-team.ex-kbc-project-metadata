'''
Template Component main class.

'''

import csv
import json
import logging
import os
import sys

import requests
from kbc.env_handler import KBCEnvHandler
from kbcstorage.base import Endpoint
from kbcstorage.tables import Tables

from result.kbc_result import TokensWriter, OrchWriter

# configuration variables
KEY_MASTER_TOKEN_KEY = '#token'
KEY_TOKEN_KEY = '#key'

KEY_GET_COMPONENT_CFGS = 'get_component_cfg_details'
KEY_GET_TR = 'get_transformations'
KEY_GET_TOKENS = 'get_tokens'
KEY_GET_ORCHS = 'get_orchestrations'
KEY_GET_TABLE = 'get_tables'
KEY_ORG_ID = 'org_id'
KEY_PROJECTID = 'project_id'
DEAFAULT_TOKEN_EXPIRATION = 1800
KEY_REGION = 'region'
KEY_MASTER_TOKEN = 'master_token'
KEY_TOKENS = 'tokens'
KEY_DATASETS = 'datasets'

OR_GROUP = [[KEY_TOKENS, KEY_MASTER_TOKEN]]

MANDATORY_PARS = [OR_GROUP]
MANDATORY_IMAGE_PARS = []

APP_VERSION = '0.0.1'

TR_CONFIG_FIELDS = [
    "project_id",
    "region",
    "tr_bucket_name",
    "tr_bucket_id",
    "tr_name",
    "tr_id",
    "phase",
    "is_disabled",
    "conf_type",
    "incremental",
    "credits_est",
    "table_size",
    "table_exists",
    "table_id",
    "table_name",
    "load_type",
    "changed_since",
    'columns',
    'total_src_cols',
    'filter',
    'last_run',
    "component_type",
    "has_description",
    "bucket_has_description",
    "no_queries"
]

COMPONENT_CONFIG_FIELDS = [
    "project_id",
    "region",
    "component_type",
    "component_id",
    "component_name",
    "config_id",
    "config_name",
    "description",
    "created_at",
    "created_by",
    "version",
    "lastUpdated_at",
    "lastUpdated_by",
    "changeDescription"
]

COMPONENT_CONFIG__INPUT_FIELDS = ['project_id', 'region', 'component_type', 'config_name', 'config_id', 'name',
                                  'row_id', 'is_disabled', 'conf_type', 'table_id', 'table_name', 'changed_since',
                                  'incremental', 'table_size', 'table_exists', 'columns', 'total_src_cols',
                                  'credits_est', 'has_description']

TABLE_CONFIG_FIELDS = [
    "project_id",
    "region",
    "id",
    "name",
    "transactional",
    "primaryKey",
    "indexedColumns",
    "created",
    "lastImportDate",
    "lastChangeDate",
    "rowsCount",
    "dataSizeBytes",
    "isAlias",
    "isAliasable",
    "attributes",
    "columns",
    "description",
    "documented_columns"
]

EMPTY_TR_TE = '-- This is a sample query.\n-- Adjust accordingly to your input mapping, output mapping\n-- ' \
              'and desired functionality.\n-- CREATE TABLE "out_table" AS SELECT * FROM "in_table";'

REGIONS = {'us-east-1': 'https://connection.keboola.com',
           'eu-central-1': 'https://connection.eu-central-1.keboola.com'}

REGION_SUFFIXES = {'us-east-1': '.keboola.com',
                   'eu-central-1': '.eu-central-1.keboola.com'}


class Component(KBCEnvHandler):

    def __init__(self, debug=False):
        KBCEnvHandler.__init__(self, MANDATORY_PARS)
        # override debug from config
        if self.cfg_params.get('debug'):
            debug = True

        # self.set_default_logger(logging.DEBUG if debug else logging.INFO)
        # fix logging before lib fix
        log_level = logging.DEBUG if debug else logging.INFO

        class InfoFilter(logging.Filter):
            def filter(self, rec):
                return rec.levelno in (logging.DEBUG, logging.INFO)

        hd1 = logging.StreamHandler(sys.stdout)
        hd1.addFilter(InfoFilter())
        hd2 = logging.StreamHandler(sys.stderr)
        hd2.setLevel(logging.WARNING)

        logging.getLogger().setLevel(log_level)
        # remove default handler
        logging.getLogger().removeHandler(logging.getLogger().handlers[0])
        logging.getLogger().addHandler(hd1)
        logging.getLogger().addHandler(hd2)

        logging.info('Running version %s', APP_VERSION)
        logging.info('Loading configuration...')

        try:
            self.validate_config()
            self.validate_image_parameters(MANDATORY_IMAGE_PARS)
        except ValueError as e:
            logging.error(e)
            exit(1)

    def run(self):
        '''
        Main execution code
        '''
        params = self.cfg_params  # noqa

        path = self.tables_out_path

        master_token = params.get(KEY_MASTER_TOKEN, [])
        datasets = params.get(KEY_DATASETS, {})
        if master_token:
            tokens = self._get_project_tokens(master_token[0], params)
        else:
            tokens = params.get(KEY_TOKENS)

        token_wr = None
        orch_writer = None
        if datasets.get(KEY_GET_TOKENS):
            # writer setup
            token_wr = TokensWriter(self.tables_out_path)
        if datasets.get(KEY_GET_ORCHS):
            orch_writer = OrchWriter(self.tables_out_path)

        for idx, tok in enumerate(tokens):
            # add token details
            tok = self.set_token_details(tok)

            if datasets.get(KEY_GET_TR):
                logging.info("Collecting transformation details for project_id %s in region %r",
                             tok[KEY_PROJECTID], tok[KEY_REGION])
                with open(os.path.join(path, 'tr_details.csv'), 'a+', newline='') as out:
                    tr_writer = csv.DictWriter(out, TR_CONFIG_FIELDS)
                    if idx == 0:
                        tr_writer.writeheader()
                    self.write_all_tr_details(tr_writer, tok)

            if datasets.get(KEY_GET_TOKENS):
                logging.info("Collecting token details")
                self.write_all_token_details(token_wr, tok)

            if datasets.get(KEY_GET_ORCHS):
                logging.info("Collecting orchestration details for project_id %s in region %r",
                             tok[KEY_PROJECTID], tok[KEY_REGION])
                test = self.get_orchestration_details(tok)
                orch_writer.write_all(test, object_from_arrays=True,
                                      user_values={'project_pk': str(tok[KEY_PROJECTID]) + '|' + tok['region']})

            if datasets.get(KEY_GET_COMPONENT_CFGS):
                logging.info("Collecting extractor details for project_id %s in region %r",
                             tok[KEY_PROJECTID], tok[KEY_REGION])
                with open(os.path.join(path, 'component_config_details.csv'), 'a+', newline='') as cfg_details, open(
                        os.path.join(path, 'component_config_inputs.csv'), 'a+', newline='') as cfg_inputs:
                    ex_writer = csv.DictWriter(cfg_details, COMPONENT_CONFIG_FIELDS)
                    input_writer = csv.DictWriter(cfg_inputs, COMPONENT_CONFIG__INPUT_FIELDS)
                    if idx == 0:
                        ex_writer.writeheader()
                        input_writer.writeheader()
                    self.write_all_component_cfg_details(ex_writer, input_writer, tok,
                                                         datasets.get(KEY_GET_COMPONENT_CFGS))

            if datasets.get(KEY_GET_TABLE):
                logging.info("Collecting table details for project_id %s in region %r",
                             tok[KEY_PROJECTID], tok[KEY_REGION])
                with open(os.path.join(path, 'table_details.csv'), 'a+', newline='') as out:
                    table_writer = csv.DictWriter(out, TABLE_CONFIG_FIELDS)
                    if idx == 0:
                        table_writer.writeheader()
                    self.write_all_table_details(table_writer, tok)

        if datasets.get(KEY_GET_TOKENS):
            logging.info("Storing manifest files.")
            self.create_manifests(
                token_wr.collect_results(), incremental=False)

        if datasets.get(KEY_GET_ORCHS):
            self.create_manifests(
                orch_writer.collect_results(), incremental=False)

    logging.info("Extraction finished")

    def get_project_tokens(self, token):
        headers = {
            'Content-Type': 'application/json',
            'X-StorageApi-Token': token[KEY_TOKEN_KEY],
        }

        response = requests.get(
            REGIONS[token[KEY_REGION]] + '/v2/storage/tokens',
            headers=headers)
        try:
            response.raise_for_status()
        except requests.HTTPError as e:
            raise e
        else:
            return response.json()

    def _get_std_token_name(self, project_name):
        return project_name + '_Telemetry_token'

    def set_token_details(self, token):
        det = self.get_token_details(token)
        token['project_id'] = det['id']
        return token

    def get_token_details(self, token):
        if REGIONS.get(token['region']):
            url = REGIONS[token['region']]
        else:
            raise ValueError('Unsupported region {}'.format(token['region']))

        token_validation = Endpoint(url, 'storage', token[KEY_TOKEN_KEY])
        token_detail = token_validation._get(
            url + '/v2/storage/tokens/verify')

        return token_detail['owner']

    def get_table_detail(self, tables: Tables, _id):
        detail = {}
        try:
            detail = tables.detail(_id)
            detail['exists'] = True
        except Exception as e:
            detail['exists'] = False
            logging.warning('Failed to get table {} [{}]'.format(_id, e))

        return detail

    def get_input_data(self, tables: Tables, cfg, r, project_id, region, component_type, last_job):
        res = []
        # backward compatibility to allow generic output/input data in the future
        if component_type == 'transformation':
            key_bucket_name = 'tr_bucket_name'
            key_bucket_id = 'tr_bucket_id'
            key_name = 'tr_name'
            key_id = 'tr_id'
        else:
            key_bucket_name = 'bucket_name'
            key_bucket_id = 'bucket_id'
            key_name = 'name'
            key_id = 'id'

        for inp in r['configuration']['input']:
            conf_row = dict()
            conf_row['project_id'] = project_id
            conf_row['region'] = region
            conf_row['component_type'] = component_type
            conf_row[key_bucket_name] = cfg['name']
            conf_row[key_bucket_id] = cfg['id']
            conf_row[key_name] = r['name']
            conf_row[key_id] = r['id']
            conf_row['phase'] = r['configuration'].get('phase', 'N/A')
            conf_row['is_disabled'] = r['configuration'].get('disabled', 'N/A')
            conf_row['conf_type'] = 'input'
            conf_row['table_id'] = inp.get('source', 'N/A')
            conf_row['table_name'] = inp.get('destination', '')
            conf_row['load_type'] = inp.get('loadType') if inp.get(
                'loadType') else 'Copy Table'
            conf_row['changed_since'] = inp.get('changedSince')
            conf_row['incremental'] = True if inp.get(
                'changedSince') else False
            tb_detail = self.get_table_detail(tables, inp.get('source', ''))
            conf_row['table_size'] = 0 if not tb_detail.get(
                'dataSizeBytes') else tb_detail.get('dataSizeBytes', 0)
            conf_row['table_exists'] = tb_detail.get('exists')
            conf_row['columns'] = len(inp.get('columns', []))
            conf_row['total_src_cols'] = len(tb_detail.get('columns', []))
            if conf_row['columns'] > 0 and conf_row['total_src_cols'] > 0:
                conf_row['credits_est'] = (int(conf_row['table_size']) / 1000000000) * (
                        conf_row['columns'] / conf_row['total_src_cols'])
            else:
                conf_row['credits_est'] = (
                        int(conf_row['table_size']) / 1000000000)
            if inp.get('whereColumn'):
                conf_row['filter'] = ' '.join(
                    [inp.get('whereColumn'), inp.get('whereOperator'), str(inp.get('whereValues'))])

            conf_row['last_run'] = last_job.get('startTime', 'N/A')
            conf_row['has_description'] = r['description'] != ''
            conf_row['bucket_has_description'] = cfg['description'] != ''
            res.append(conf_row)
        return res

    def get_storage_input_data(self, tables: Tables, cfg, r, project_id, region, component_type):
        res = []
        key_bucket_name = 'config_name'
        key_bucket_id = 'config_id'
        key_name = 'name'
        key_id = 'row_id'

        for inp in r.get('configuration', {}).get('storage', {}).get('input', {}).get('tables', []):
            conf_row = dict()
            conf_row['project_id'] = project_id
            conf_row['region'] = region
            conf_row['component_type'] = component_type
            conf_row[key_bucket_name] = cfg['name']
            conf_row[key_bucket_id] = cfg['id']
            conf_row[key_name] = r.get('name', 'N/A')
            conf_row[key_id] = r.get('id', 'N/A')
            conf_row['is_disabled'] = r.get('configuration', {}).get('disabled', 'N/A')
            conf_row['conf_type'] = 'input'
            conf_row['table_id'] = inp.get('source', 'N/A')
            conf_row['table_name'] = inp.get('destination', '')
            conf_row['changed_since'] = inp.get('changed_since')
            conf_row['incremental'] = True if inp.get(
                'changed_since') else False
            tb_detail = self.get_table_detail(tables, inp.get('source', ''))
            conf_row['table_size'] = 0 if not tb_detail.get(
                'dataSizeBytes') else tb_detail.get('dataSizeBytes', 0)
            conf_row['table_exists'] = tb_detail.get('exists')
            conf_row['columns'] = len(inp.get('columns', []))
            conf_row['total_src_cols'] = len(tb_detail.get('columns', []))
            if conf_row['columns'] > 0 and conf_row['total_src_cols'] > 0:
                conf_row['credits_est'] = (int(conf_row['table_size']) / 1000000000) * (
                        conf_row['columns'] / conf_row['total_src_cols'])
            else:
                conf_row['credits_est'] = (
                        int(conf_row['table_size']) / 1000000000)
            if inp.get('whereColumn'):
                conf_row['filter'] = ' '.join(
                    [inp.get('whereColumn'), inp.get('whereOperator'), str(inp.get('whereValues'))])
            conf_row['has_description'] = r.get('description', '') != ''
            res.append(conf_row)
        return res

    def get_output_data(self, tables: Tables, cfg, r, project_id, region, component_type, last_job):
        res = []
        # backward compatibility to allow generic output/input data in the future
        if component_type == 'transformation':
            key_bucket_name = 'tr_bucket_name'
            key_bucket_id = 'tr_bucket_id'
            key_name = 'tr_name'
            key_id = 'tr_id'
        else:
            key_bucket_name = 'bucket_name'
            key_bucket_id = 'bucket_id'
            key_name = 'name'
            key_id = 'id'

        for inp in r['configuration']['output']:
            conf_row = dict()
            conf_row['project_id'] = project_id
            conf_row['region'] = region
            conf_row['component_type'] = component_type
            conf_row[key_bucket_name] = cfg['name']
            conf_row[key_bucket_id] = cfg['id']
            conf_row[key_name] = r['name']
            conf_row[key_id] = r['id']
            conf_row['phase'] = r['configuration'].get('phase', 'N/A')
            conf_row['is_disabled'] = r['configuration'].get('disabled', 'N/A')
            conf_row['conf_type'] = 'output'
            conf_row['table_id'] = inp['destination']
            conf_row['table_name'] = inp['source']
            conf_row['load_type'] = 'incremental' if inp.get(
                'incremental') else 'full'
            conf_row['changed_since'] = ''
            conf_row['incremental'] = inp.get(
                'incremental') if inp.get('incremental') else False
            tb_detail = self.get_table_detail(tables, inp.get('destination', ''))
            conf_row['table_size'] = 0 if not tb_detail.get(
                'dataSizeBytes') else tb_detail.get('dataSizeBytes', 0)
            conf_row['table_exists'] = tb_detail.get('exists')
            conf_row['credits_est'] = int(conf_row['table_size']) / 1000000000
            conf_row['last_run'] = last_job.get('startTime', 'N/A')
            conf_row['has_description'] = r['description'] != ''
            conf_row['bucket_has_description'] = cfg['description'] != ''
            res.append(conf_row)
        return res

    def get_transformation_detail_data(self, tables, components, bucket_id, token):

        project_id = token['project_id']
        region = token['region']
        cfg = components._get(
            components.base_url + '/transformation/configs/' + bucket_id)
        rows = cfg['rows']
        component_type = 'transformation'

        res = []
        for r in rows:
            inputs, outputs = None, None

            last_job = self._get_last_config_job(token[KEY_TOKEN_KEY], REGION_SUFFIXES[token['region']],
                                                 'transformation', bucket_id)
            if r.get('configuration', {}).get('input'):
                inputs = self.get_input_data(
                    tables, cfg, r, project_id, region, component_type, last_job)
                res.extend(inputs)
            if r.get('configuration', {}).get('output'):
                outputs = self.get_output_data(
                    tables, cfg, r, project_id, region, component_type, last_job)
                res.extend(outputs)
            if not inputs and not outputs:
                res.append(
                    {'tr_bucket_name': cfg['name'],
                     'tr_bucket_id': cfg['id'],
                     'tr_name': r['name'],
                     'tr_id': r['id'],
                     'conf_type': 'no_in_out',
                     'table_id': '',
                     'table_name': '',
                     'load_type': '',
                     'changed_since': '',
                     'incremental': '',
                     'filter': '',
                     'last_run': last_job.get('startTime', 'N/A'),
                     'is_disabled': r['configuration'].get('disabled', 'N/A'),
                     'table_size': '',
                     'table_exists': '',
                     'region': region,
                     'project_id': project_id,
                     'component_type': component_type,
                     'has_description': r['description'] != '',
                     'bucket_has_description': cfg['description'] != '',
                     'no_queries': ''.join(r.get('configuration', {}).get('queries', [])).strip() != ''
                     })

        return res

    def write_all_tr_details(self, writer, token):
        if REGIONS.get(token['region']):
            url = REGIONS[token['region']]
        else:
            raise ValueError('Unsupported region {}'.format(token['region']))

        sourceEndpoint = Endpoint(url, 'components', token[KEY_TOKEN_KEY])
        tb = Tables(url, token[KEY_TOKEN_KEY])
        all_cfgs = sourceEndpoint._get(
            url + '/v2/storage/components/transformation/configs')

        for cfg in all_cfgs:
            writer.writerows(
                self.get_transformation_detail_data(tb, sourceEndpoint, cfg['id'], token))

    def get_component_config_detail_data(self, components, project_id, region, tables):
        rows = components["configurations"]
        cfg = {
            "project_id": project_id,
            "region": region,
            "component_type": components["type"],
            "component_id": components["id"],
            "component_name": components["name"]
        }

        res = []
        rows_input = []
        for r in rows:
            row_cfg = cfg
            row_cfg["config_id"] = r["id"]
            row_cfg["config_name"] = r["name"]
            row_cfg["description"] = r["description"]
            row_cfg["created_at"] = r["created"]
            row_cfg["created_by"] = r["creatorToken"]["description"]
            row_cfg["version"] = r["version"]
            row_cfg["lastUpdated_at"] = r["currentVersion"]["created"]
            row_cfg["lastUpdated_by"] = r["currentVersion"]["creatorToken"]["description"]
            row_cfg["changeDescription"] = r["changeDescription"]
            res.append(row_cfg)
            rows_input.extend(self.get_storage_input_data(tables, r, r, project_id, region, components["type"]))
            for cfgrow in r.get('rows', []):
                rows_input.extend(
                    self.get_storage_input_data(tables, r, cfgrow, project_id, region, components["type"]))
        return res, rows_input

    def write_all_component_cfg_details(self, detail_writer, input_writer, token, component_types):
        if REGIONS.get(token['region']):
            url = REGIONS[token['region']]
        else:
            raise ValueError('Unsupported region {}'.format(token['region']))
        sourceEndpoint = Endpoint(url, 'components', token[KEY_TOKEN_KEY])
        tb = Tables(url, token[KEY_TOKEN_KEY])
        for comp_type in component_types:
            all_cfgs = sourceEndpoint._get(
                url + '/v2/storage/components', params={"componentType": comp_type, "include": "rows,configuration"})

            for cfg in all_cfgs:
                cfg_details, rowinputs = self.get_component_config_detail_data(cfg, token['project_id'],
                                                                               token['region'], tb)
                detail_writer.writerows(cfg_details)
                input_writer.writerows(rowinputs)

    def get_all_table_details_data(self, table_details, region, project_id):
        res = []
        data = {
            'project_id': project_id,
            'region': region,
            'id': table_details['id'],
            'name': table_details['name'],
            'transactional': table_details['transactional'],
            'primaryKey': ', '.join(table_details['primaryKey']),
            'indexedColumns': ', '.join(table_details['indexedColumns']),
            'created': table_details['created'],
            'lastImportDate': table_details['lastImportDate'],
            'lastChangeDate': table_details['lastChangeDate'],
            'rowsCount': table_details['rowsCount'],
            'dataSizeBytes': table_details['dataSizeBytes'],
            'isAlias': table_details['isAlias'],
            'isAliasable': table_details['isAliasable'],
            # isn't this field deprecated?
            'attributes': str(table_details['attributes']),
            'columns': ', '.join(table_details['columns']),
            'documented_columns': ', '.join(self.get_col_meta_documented_cols(table_details['columnMetadata'])),
            'description': self.get_meta_description(table_details['metadata'])
        }
        res.append(data)

        return res

    def get_col_meta_documented_cols(self, colmeta):
        doc_cols = []
        for c in colmeta:
            if self.get_meta_description(colmeta[c]):
                doc_cols.append(c)
        return doc_cols

    def get_meta_description(self, metadata):
        for m in metadata:
            if m.get('key', '') == 'KBC.description':
                return m['value']

    def write_all_table_details(self, writer, token):
        if REGIONS.get(token['region']):
            url = REGIONS[token['region']]
        else:
            raise ValueError('Unsupported region {}'.format(token['region']))

        tb = Tables(url, token[KEY_TOKEN_KEY])
        args = ["attributes", "columns", "metadata", "columnMetadata"]
        all_tb = tb.list(include=args)

        for table in all_tb:
            writer.writerows(self.get_all_table_details_data(
                table, token['project_id'], token['region']))

    def generate_token(self, decription, token, proj_id, region, expires_in=DEAFAULT_TOKEN_EXPIRATION):
        headers = {
            'Content-Type': 'application/json',
            'X-KBC-ManageApiToken': token,
        }

        data = {
            "description": decription,
            "canManageBuckets": True,
            "canReadAllFileUploads": False,
            "canPurgeTrash": False,
            "canManageTokens": True,
            "componentAccess": ["transformation"],
            "bucketPermissions": {"*": "read"},
            "expiresIn": expires_in
        }

        response = requests.post(REGIONS[region] + '/manage/projects/' + str(proj_id) + '/tokens',
                                 headers=headers,
                                 data=json.dumps(data))
        try:
            response.raise_for_status()
        except requests.HTTPError as e:
            raise e
        else:
            return response.json()

    def get_organization(self, master_token):
        headers = {
            'Content-Type': 'application/json',
            'X-KBC-ManageApiToken': master_token[KEY_MASTER_TOKEN_KEY],
        }

        response = requests.get(
            REGIONS[master_token[KEY_REGION]] +
            '/manage/organizations/' + str(master_token[KEY_ORG_ID]),
            headers=headers)
        try:
            response.raise_for_status()
        except requests.HTTPError as e:
            raise e
        else:
            return response.json()

    def get_orchestrations(self, token_key, url_suffix):
        syrup_cl = Endpoint('https://syrup' + url_suffix,
                            'orchestrator', token_key)

        url = syrup_cl.root_url + '/orchestrator/orchestrations'
        res = syrup_cl._get(url)
        return res

    def get_orch_detail(self, config_id, token_key, url_suffix):
        cl = Endpoint('https://connection' + url_suffix,
                      'components', token_key)
        url = '{}/{}/configs/{}'.format(cl.base_url, 'orchestrator', config_id)
        return cl._get(url)

    def get_orchestration_details(self, token):
        url_suffix = REGION_SUFFIXES[token['region']]
        orchs = self.get_orchestrations(token[KEY_TOKEN_KEY], url_suffix)

        details = []
        for o in orchs:
            orch_detail = self.get_orch_detail(
                o['id'], token[KEY_TOKEN_KEY], url_suffix)
            last_jobs = self._get_last_orch_job(
                token[KEY_TOKEN_KEY], url_suffix, o['id'])
            orch_detail['last_run'] = last_jobs.get('startTime')
            details.append(orch_detail)
        return details

    def _get_project_tokens(self, master_token, params):
        # get list of projects
        org = self.get_organization(master_token)
        projects = org['projects']
        for proj in projects:
            # try create token
            tok = self.generate_token(self._get_std_token_name(proj['name']), master_token[KEY_MASTER_TOKEN_KEY],
                                      proj['id'],
                                      master_token[KEY_REGION])
            # add token details
            tok[KEY_REGION] = master_token[KEY_REGION]
            tok[KEY_PROJECTID] = proj['id']
            tok[KEY_TOKEN_KEY] = tok['token']
            yield tok

    def _get_n_write_all_in_org(self, writer, master_token, params):
        # get list of projects
        org = self.get_organization(master_token)
        projects = org['projects']
        for proj in projects:
            # try create token
            tok = self.generate_token(self._get_std_token_name(proj['name']), master_token['token'], proj['id'],
                                      master_token[KEY_REGION])
            # add token details
            tok[KEY_REGION] = master_token[KEY_REGION]
            tok[KEY_PROJECTID] = proj['id']
            tok[KEY_TOKEN_KEY] = tok['token']

            logging.info(
                "Collecting TR details for project_id %s in region %r", proj['id'], tok[KEY_REGION])
            self.write_all_tr_details(writer, tok)

            if params.get(KEY_TOKENS):
                logging.info(
                    "Collecting token details for project_id %s in region %r", proj['id'], tok[KEY_REGION])
                self.write_all_token_details(tok)

    def write_all_token_details(self, writer, tok):
        res = self.get_project_tokens(tok)
        for t in res:
            last_event = self._get_token_last_event(tok, t['id'])
            t["last_event_date"] = last_event.get('created')
            t["last_event"] = last_event.get('event')

        writer.write_all(res, object_from_arrays=True,
                         user_values={'project_pk': str(tok[KEY_PROJECTID]) + '|' + tok['region']})

    def _get_token_last_event(self, token, token_id):
        headers = {
            'Content-Type': 'application/json',
            'X-StorageApi-Token': token[KEY_TOKEN_KEY],
        }
        params = dict()
        params['limit'] = 1
        response = requests.get(
            REGIONS[token[KEY_REGION]] +
            '/v2/storage/tokens/' + token_id + '/events',
            headers=headers, params=params)
        try:
            response.raise_for_status()
        except requests.HTTPError as e:
            logging.warning(F'Failed to get token events {token_id}. {e}')
            raise e
        else:
            json_resp = response.json()
            if not json_resp:
                return dict()
            else:
                return json_resp[0]

    def _get_last_orch_job(self, token_key, url_suffix, orch_id):
        headers = {
            'Content-Type': 'application/json',
            'X-StorageApi-Token': token_key,
        }
        params = dict()
        params['limit'] = 1
        response = requests.get(
            'https://syrup' + url_suffix +
            '/orchestrator/orchestrations/' + str(orch_id) + '/jobs',
            headers=headers, params=params)
        try:
            response.raise_for_status()
        except requests.HTTPError as e:
            raise e
        else:
            json_resp = response.json()
            if not json_resp:
                return dict()
            else:
                return json_resp[0]

    def _get_last_config_job(self, token_key, url_suffix, component_type, config_id):
        headers = {
            'Content-Type': 'application/json',
            'X-StorageApi-Token': token_key,
        }
        params = dict()
        params['limit'] = 1
        params['q'] = F'(component:{component_type} OR params.component:{component_type}) AND params.config:{config_id}'
        response = requests.get(
            'https://syrup' + url_suffix + '/queue/jobs',
            headers=headers, params=params)
        try:
            response.raise_for_status()
        except requests.HTTPError as e:
            raise e
        else:
            json_resp = response.json()
            if not json_resp:
                return dict()
            else:
                return json_resp[0]

    def _get_last_job(self, token_key, url_suffix, config_id):
        headers = {
            'Content-Type': 'application/json',
            'X-StorageApi-Token': token_key,
        }
        params = dict()
        params['limit'] = 1
        params['q'] = '+params.config:' + config_id
        response = requests.get(
            'https://syrup' + url_suffix + '/queue/jobs',
            headers=headers, params=params)
        try:
            response.raise_for_status()
        except requests.HTTPError as e:
            raise e
        else:
            json_resp = response.json()
            if not json_resp:
                return dict()
            else:
                return json_resp[0]


"""
        Main entrypoint
"""
if __name__ == "__main__":
    if len(sys.argv) > 1:
        debug = sys.argv[1]
    else:
        debug = False
    comp = Component(debug)
    comp.run()
