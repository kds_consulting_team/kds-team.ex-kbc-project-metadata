from kbc.result import KBCTableDef
from kbc.result import ResultWriter

TOKEN_PK = ['id', 'project_pk']

TOKEN_COLS = ["id", "description", "created", "refreshed", "uri", "isMasterToken", "canManageBuckets",
              "canManageTokens", "canReadAllFileUploads", "canPurgeTrash", "isExpired", "isDisabled",
              "dailyCapacity", "kbc_user_id", "project_pk", "last_event", "last_event_date"]

ORCH_PK = ['id', 'project_pk']
ORCH_COLS = ["id", "name", "description", "created", "is_deleted", "active", "project_pk", "last_run"]


class TokensWriter(ResultWriter):
    """
    Overridden constructor method of ResultWriter. It creates extra ResultWriter instance that handles processing of
    the nested object 'deals_stage_history'. That writer is then called from within the write method.
    """

    def __init__(self, out_path, buffer=8192):
        # specify result table
        # table def
        token_table = KBCTableDef(name='tokens', pk=TOKEN_PK, columns=TOKEN_COLS)
        # writer setup
        ResultWriter.__init__(self, result_dir_path=out_path, table_def=token_table, flatten_objects=False,
                              fix_headers=True, exclude_fields=['token'])

        permission_tb = KBCTableDef('token_bucket_permissions', [], ['bucket', 'project_pk'])
        self.permission_writer = ResultWriter(out_path, permission_tb,
                                              flatten_objects=False, buffer_size=buffer)

    """
    Overridden write method that is modified to process the nested object separately using newly created nested writer.
    """

    def write(self, data, file_name=None, user_values=None, object_from_arrays=True, write_header=True):
        # write ext users
        permissions = data.pop('bucketPermissions')
        if permissions:
            bucket_permissions = self.dict_to_array(permissions, user_values['project_pk'])
            self.permission_writer.write_all(bucket_permissions)
            self.results = {**self.results, **self.permission_writer.results}
        # get user id
        if data.get('creatorToken'):
            uid = data['creatorToken']['id']
            data.pop('creatorToken')
        elif data.get('admin'):
            uid = data['admin']['id']
            data.pop('admin')
        else:
            uid = 'N/A'
        user_values['kbc_user_id'] = uid
        super().write(data, user_values=user_values, object_from_arrays=object_from_arrays)

    def dict_to_array(self, dict, id):
        res_list = list()
        for k in dict:
            res_list.append({"bucket": k, "permission": dict[k], "project_pk": id})
        return res_list


class OrchWriter(ResultWriter):
    """
    Overridden constructor method of ResultWriter. It creates extra ResultWriter instance that handles processing of
    the nested object 'deals_stage_history'. That writer is then called from within the write method.
    """

    def __init__(self, out_path, buffer=8192):
        # specify result table
        # table def
        orch_table = KBCTableDef(name='orchestrations', pk=ORCH_PK, columns=ORCH_COLS)
        # writer setup
        ResultWriter.__init__(self, result_dir_path=out_path, table_def=orch_table, flatten_objects=True,
                              fix_headers=True,
                              exclude_fields=['token', 'state', 'currentVersion', 'rows', 'rowsSortOrder',
                                              'notifications'])

        tasks_tb = KBCTableDef('orchestration_tasks', [], ['orchestration_id', 'project_pk', 'id'])
        self.tasks_writer = ResultWriter(out_path, tasks_tb,
                                         flatten_objects=False, buffer_size=buffer)

    """
       Overridden write method that is modified to process the nested object separately using newly created
       nested writer.
       """

    def write(self, data, file_name=None, user_values=None, object_from_arrays=True, write_header=True):
        # write ext users
        tasks = data.get('configuration').get('tasks')
        if tasks:
            for t in tasks:
                t['config_id'] = t.pop('actionParameters', {}).get('config')
                t['orchestration_id'] = data['id']
                self.tasks_writer.write(t, user_values=user_values)
            self.results = {**self.results, **self.tasks_writer.results}

        data.pop('configuration')
        super().write(data, user_values=user_values, object_from_arrays=object_from_arrays)
