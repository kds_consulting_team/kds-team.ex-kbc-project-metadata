Analyse transformation configurations in your projects. See what tables are used on the input, 
what input mapping setting is being used and what may cause unnecessary increased I/O usage.