**NOTE:** When in mode with `Master token`, the application creates a temporary Storage token in the destination project that has access to `all buckets` and components and
is named `[project_name]_Telemetry_token`. The token expires after `30 Minutes`.

