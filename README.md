# KBC project metadata extractor

A custom KBC component that allows scanning project Transformations, Orchestrations, Tokens and others and retrieving metadata useful for on-demand health check analysis.

A helpful tool for quantitative health check analysis. Discover unused code, I/O, documentation coverage and more.

As any project grows it is a good idea to perform a regular reviews of its state, in particular over time there may be introduced some processes that are not efficiently designed and consume a lot of time and credit resources. This tool helps identifying transformations that are eligible for possible optimization in terms of more effective incremental processing. But also identifying configurations that contain non-existent tables on the inputs or inconsistency in the input mapping.

Lot of the indicators of a ineffectively designed transformations may be identified from the Input / Output mapping of the configuration itself.

In general these are:

- Copy or Clone mode?
- Is there any column filters?
- Is there any data filters?
- Is there incremental setup (Changed in Last x)
- Is there incremental output
- How large the tables on the input are and what is the estimated credit consumption?

All above can be retrieved using this extractor.

## Setup

To create a new configuration enter URL:

[https://connection.eu-central-1.keboola.com/admin/projects/[PROJECT\_ID]/extractors/kds-team.ex-kbc-project-metadata](https://connection.eu-central-1.keboola.com/admin/projects/196/extractors/kds-team.ex-kbc-project-metadata)

 and replace the [PROJECT\_ID] with ID of the project you want the configuration in.
## Output result

Extractor outputs following tables:

- **tr_details** - details of transformation configs and its' I/O mappings
- **orchestrations** - orchestration metadata, last run, activity
- **orchestration_tasks** - tasks of orchestrations
- **tokens** - Storage tokens present in the project, last activity, permissions
- **token_bucket_permissions** - bucket permissions of tokens
- **tokens_componentAccess**
- **component_config_details** - component config metadata (last modified, state, creator, etc)
- **table_details** - Storage tables metadata (created, updated, size, cols, 
description coverage, etc)

**TR_details column description:**

| **project\_id** | project id |
| --- | --- |
| **region** | project region |
| **tr\_bucket\_name** | Bucket name |
| **tr\_bucket\_id** | [https://connection.eu-central-1.keboola.com/admin/projects/project\_id/transformations/bucket/](https://connection.eu-central-1.keboola.com/admin/projects/project_id/transformations/bucket/)bucket\_id/transformation/tr\_id |
| **tr\_name** | Transformation name |
| **tr\_id** | [https://connection.eu-central-1.keboola.com/admin/projects/project\_id/transformations/bucket/bucket\_id/transformation/](https://connection.eu-central-1.keboola.com/admin/projects/project_id/transformations/bucket/bucket_id/transformation/)tr\_id |
| **conf\_type** | Defines type of output mapping [Input, output] |
| **incremental** | Is incremental? [False, True] |
| **credits\_est** | Estimated credit consumption. Calculated using table size 1GB = 1CR (In reality this might be more)  In case on subset of columns used on input the number is adjusted according the ratio of the column number used (filtered\_cols / total\_cols ). NOTE: this number is an approximation and does not reflect the filters applied and actual column sizes. In case of output mapping the number is always the actual size of the output table (incorrect in case of incremental output) |
| **table\_size** | Table size in Bytes |
| **table\_exists** | table exists |
| **is\_disabled** |  |
| **table\_id** | E.g. in.c-codelists\_manual\_input.CALENDAR |
| **load\_type** | Load type of the tabel: Input mapping [Copy Table, Clone Table]; Output mapping [full, incremental] |
| **changed\_since** | Changed since filter if specified. E.g. 1 day ago |
| **columns** | Number of filtered columns. If all columns are used, the value is 0 |
| **total\_src\_cols** | Total number of columns in the source table. (0 if table does not exist) |
| **filter** | Filter on input mapping if applies. E.g. department eq [], COM\_DIR eq [&#39;0&#39;] |
| **table\_exists** | Does table exist or not [TRUE,FALSE](../../C:%5Cpages%5Ccreatepage.action%3FspaceKey=BIEXT&amp;title=TRUE,FALSE&amp;linkCreation=true&amp;fromPageId=62768246) |

## **Configuration**

The app works in two modes:

Providing **Master Token:**

Once the master token is provided, the application automatically scans all projects in specified organization that the token has access to. The app scans through each project
in specified organisation, creates a temporary token named `[project_name]_Telemetry_token` with validity for 30 minutes.

To get the master token, you need to be an organisation admin and create one by navigating to [https://connection.eu-central-1.keboola.com/admin/account/access-tokens](https://connection.eu-central-1.keboola.com/admin/account/access-tokens) and clicking the &quot;New token&quot; token button.


You need to fill following configuration fields:

- **Token key ** - your generated master token
- **Organization id -**  Id of the organisation you want to scan ([https://connection.eu-central-1.keboola.com/admin/organizations/](https://connection.eu-central-1.keboola.com/admin/organizations/77)[**[**](https://connection.eu-central-1.keboola.com/admin/organizations/77)**ORGANISATION\_ID]**)
- **Project region - ** region of you organisation / projects

**NOTE: ** Specifying master token will override any configuration you have in the &quot;_List of Storage tokens_&quot; section



Providing **List of Storage tokens** explicitly:

In this mode the app will go through each project specified by its Storage token and region.
You need to fill following configuration fields:

- **Token key** - your generated master token
- **Project region** -  region of you organisation / projects
- **Project ID / Name** - An optional column, for UI reference about the token. 

### Supported metadata

Specify **Project data to download** parameter to pick what data you want to fetch.tokens

**Supported:**

- Transformation details
- Token details
- Orchestration details
- Component config metadata \[extractors, writers, applications\]
- Table details






## Development

This example contains runnable container with simple unittest. For local testing it is useful to include `data` folder in the root
and use docker-compose commands to run the container or execute tests.

If required, change local data folder (the `CUSTOM_FOLDER` placeholder) path to your custom path:
```yaml
    volumes:
      - ./:/code
      - ./CUSTOM_FOLDER:/data
```

Clone this repository, init the workspace and run the component with following command:

```
git clone https://bitbucket.org:kds_consulting_team/kbc-python-template.git my-new-component
cd my-new-component
docker-compose build
docker-compose run --rm dev
```

Run the test suite and lint check using this command:

```
docker-compose run --rm test
```

# Integration

For information about deployment and integration with KBC, please refer to the [deployment section of developers documentation](https://developers.keboola.com/extend/component/deployment/)